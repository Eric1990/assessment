using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Management;

namespace discInfo
{
    public partial class Form1 : Form
    {
        private const long MB = 1048576; //  1024 * 1024 = 1048576
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear(); // Clear listView1 
            ListViewItem listViewItem;
            ManagementClass diskClass = new ManagementClass("Win32_LogicalDisk");
            ManagementObjectCollection disks = diskClass.GetInstances();
            foreach (ManagementObject disk in disks)
            {
                try
                {
                    // Disc Name
                    listViewItem = new ListViewItem(disk["Name"].ToString(), 0);
                    // Disc Description
                    listViewItem.SubItems.Add(disk["Description"].ToString());
                    // Disc storage occupancy, used space, freeSpace 
                    if (System.Convert.ToInt64(disk["Size"]) > 0)
                    {
                        long totalSpace = System.Convert.ToInt64(disk["Size"]) / MB;
                        long freeSpace = System.Convert.ToInt64(disk["FreeSpace"]) / MB;
                        long usedSpace = totalSpace - freeSpace;
                        listViewItem.SubItems.Add(totalSpace.ToString());
                        listViewItem.SubItems.Add(usedSpace.ToString());
                        listViewItem.SubItems.Add(freeSpace.ToString());
                    }
                    listView1.Items.Add(listViewItem);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "MSG");
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();  // Close the window
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
                label2.Text = dlg.SelectedPath.ToString();
        }
       
    }
}

